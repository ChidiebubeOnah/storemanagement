﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using StoreManagementSystem.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace StoreManagementSystem.Models
{
    public class ShoppingCart
    {
        private readonly ApplicationDbContext _db;
        public ShoppingCart(ApplicationDbContext db, HttpContext context)
        {
            _db = db;
            ShoppingCartId = GetCartId(context);
        }

        
        private string ShoppingCartId { get; set; }
        public const string CartSessionKey = "Id";
       
        //Helper method to simplify shopping cart calls

        public void AddToCart(Product product)
        {
            //get the matching cart
            var cartItem = _db.Carts.SingleOrDefault(c => c.Id == ShoppingCartId && c.ProductId == product.Id);
            if (cartItem == null)
            {
                
                //create a new cart item if no cart exists
                cartItem = new Cart
                {
                    ProductId = product.Id,
                    Id = ShoppingCartId,
                    Quantity = 1,
                    DateCreated = DateTime.Now
                };
                _db.Carts.Add(cartItem);
            }
            else
            {
                //if the item does exist in thecart, then add one to the quantity
                cartItem.Quantity++;
            }
            //save changes
            _db.SaveChanges();
        }

        public int RemoveFromCart(int id)
        {
            //get the cart
            var cartItem = _db.Carts.Single(cart => cart.Id == ShoppingCartId && cart.RecordId == id);
            int itemCount = 0;
            if (cartItem != null)
            {
                if (cartItem.Quantity > 1)
                {
                    cartItem.Quantity--;
                    itemCount = cartItem.Quantity;
                }
                else
                {
                    _db.Carts.Remove(cartItem);
                }
                //save changes
                _db.SaveChanges();
            }
            return itemCount;
        }
        public void EmptyCart()
        {
            var cartItems = _db.Carts.Where(cart => cart.Id == ShoppingCartId);
            foreach (var item in cartItems)
            {
                _db.Carts.Remove(item);
            }
            //savechanges
            _db.SaveChanges();
        }
        public List<Cart> GetCartItems()
        {
            return _db.Carts.Where(cart => cart.Id == ShoppingCartId).ToList();
        }
        public int GetCount()
        {
            //get the count of each item in the cart and sum them up
            int? count = (from cartItems in _db.Carts
                          where cartItems.Id == ShoppingCartId
                          select (int?)cartItems.Quantity).Sum();
            //return 0 if all enteries are null
            return count ?? 0;
        }
        public double GetTotal()
        {
            //multiply product price by count of that product to get the current price for each of those products in the cart
            //sum all album price totaals to get the cart total
            double? total = (from cartItems in _db.Carts
                              where cartItems.Id == ShoppingCartId
                              select (int?)cartItems.Quantity * cartItems.Product.ProductUnitPrice).Sum();
            return total ?? 0.0;
        }
        public int CreateOrder(Order order)
        {
            double orderTotal = 0;
            var cartItems = GetCartItems();
            //iterate over the items in the cart, adding the order details for each
            foreach (var item in cartItems)
            {
                var orderDetail = new OrderDetail
                {
                    ProductID = item.ProductId,
                    OrderId = order.OrderId,
                    UnitPrice = item.Product.ProductUnitPrice,
                    Quantity = item.Quantity,
                };

                //set the order total of the shopping cart
                orderTotal += (item.Quantity * item.Product.ProductUnitPrice);
                _db.OrderDetails.Add(orderDetail);
            }
            //set the order's total to the orderTotal count
            order.Total = orderTotal;
            //save the order
            _db.SaveChanges();
            //empty the shopping cart
            EmptyCart();
            //return the OrderId as the confirmation number
            return order.OrderId;
        }

        //we are using HttpContext to allow access to cookies
        public string GetCartId(HttpContext context)
        {
            if (context.Session.GetString(CartSessionKey) == null)
            {
                if (!string.IsNullOrWhiteSpace(context.User.Identity.Name))
                {
                    context.Session.SetString(CartSessionKey, context.User.Identity.Name);
                }
                else
                {
                    //generate a new random GUID using System.Guid class
                    Guid tempCartId = Guid.NewGuid();
                    //send tempCartId back to client as a cookie
                    context.Session.SetString(CartSessionKey, tempCartId.ToString());
                }
            }
            return context.Session.GetString(CartSessionKey);
        }
        //when a user has logged in, migrate their shopping cart to be associated with their username
        public void MigrateCart(string userName)
        {
            var shoppingCart = _db.Carts.Where(c => c.Id == ShoppingCartId);
            foreach (Cart item in shoppingCart)
            {
                item.Id = userName;
            }
            _db.SaveChanges();
        }
    }
}
